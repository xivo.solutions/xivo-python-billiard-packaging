# The packaging information for python-billiard in XiVO

This repository contains the packaging information for
[python-billiard](https://pypi.python.org/pypi/billiard).

To get a new version of python-billiard in the XiVO repository, set the desired
version in the `VERSION` file, update the md5sum file and increment the
changelog. [Jenkins](jenkins.xivo.io) will then retrieve and build the new version.

The version in the changelog should follow the following scheme
*<upstream-version>-1~xivo<n>* where upstream version is the version in the
VERSION file and n is the version of the package. The *-1* is to be sure we can
use the Debian version when we update to Jessie without to much headache.

This package will be obsolete when XiVO is updated to Debian Jessie.
